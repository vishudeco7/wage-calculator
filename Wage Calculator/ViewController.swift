//
//  ViewController.swift
//  Wage Calculator
//
//  Created by Vishwajith on 09/12/16.
//  Copyright © 2016 vishudeco7. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var genderControl: UISegmentedControl!
    @IBOutlet weak var skilledSwitch: UISwitch!
    @IBOutlet weak var daysWorkedTxt: UITextField!
    @IBOutlet weak var totalWagesLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func calculateWages(_ sender: AnyObject) {
        
        var costPerDay: Double = 0
        var skilledExtra: Double = 0
        var totalWages: Double
        
            if nameTxt.text! == "" || (genderControl.isEnabledForSegment(at: 0) == false) && (genderControl.isEnabledForSegment(at: 1) == false) || daysWorkedTxt.text! == "" {
                print("Please Enter all Fields")
            } else {
                if genderControl.isEnabledForSegment(at: 0) {
                    costPerDay = 140
                } else {
                    costPerDay = 160
                }
        
                if skilledSwitch.isOn {
                    skilledExtra = 50
                }
        
                totalWages = costPerDay + skilledExtra
                totalWagesLbl.text = "\(totalWages)"
        }
        
    }

}

